# gehenna template engine

good luck, suckers.

what is gehenna? it is TERRIBLE. but beyond that, it's a templating system for HTML designed along the lines of LaTeX (and despite how horrifying this code is, I still find it infinitely preferable to the bloated festering mess that is LaTeX)

## usage
`./gehenna.rb file.gh > file.html` will compile `file.gh` to `file.html`. 


## syntax
define a macro:

``:[name][output]`

call a macro:

``name` => "output"

parameterized macro:

``:[macro][2][one: {#1} two: {#2}]`

i have no idea if macros support recursion, but you can put macros inside others, both calls and definitions. nested definitions are scoped properly. the mechanism that permits this is just REMARKABLE

    `:[macro][2][
    	`:[innermacro][1][hiya, {\\#1}!]
    	"`innermacro[{#1}]" screams the {#2} template engine
    ]
    `macro[Mbogo][horrible]
    
    `macro[Mbogo][murderous]

there aren't really comments but you can toggle output with ``@`

to link a css file, use ``css[style.css]`

to declare a global variable, use ```.[var-name]`` or ``.[var-name][initial-value]`. variables must be declared before used. they are untyped. i'm sorry, were you expecting type safety? that was foolish of you.

to read a variable, do

    `var-name

or

    `?[var-name]

the latter allows macro expansion. variable variables are the closest you get to indirection and i laugh at your suffering

to set a variable, use ```=[var][value]``.

to clear a variable, use ```0[var]``. shortcut for ``=[var][]`.

to increment a variable, use ```+[var]`` to increment by one or ``+[var][amt]`. you can also do -, *, /, or ^ this way. do you think this is the 21st century and we're supposed to be beyond asm-style math? you should be grateful you get that much you elitist fucking prick

to parse another file, use ``#[file]`. it'll be a sub-unit so you wont have access to any of its macros, but it can access yours. do you think that's unfair? you're a fucking commie

to include another file w/o parsing it, use ``##[file]`. to parse it with the rest of the doc, idk, probably use `cat` or something, i don't care.

to replace text, use ```replace[haystack][needle][replacement]``. so ``replace[better dead than green][green][red]` produces exactly what you think it does. why do you hate america

## formatting

paragraphs are separated by two newlines. to prevent a line from being wrapped in <p>, prefix it with `~`. but it won't take back any of the times you were picked on in high school

a table of contents will automatically be built at the top of the file as a div.toc populated with a[href]s. style it with css. or don't. see if I care

links take the form of `[title](address)`.

_never forget: you deserved this._