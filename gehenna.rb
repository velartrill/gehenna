#!/usr/bin/env ruby

# copyright 2014 alexis summer hale. released under the 3-clause BSD
# license, despite my misgivings that I should keep it proprietary
# to protect the mental health of others

document = File.read(ARGV[0])
def html(b,css)
	return '<!DOCTYPE html>
<html>
<head>
<style>
'+(css != nil ? File.read(css) : '' )+'</style>
<body>
<div id="text">
'+b+'</div></body>
</html>'
end

def wrap(tag,text)
	'<'+tag+'>'+text+'</'+tag+'>'
end

def mathop(v1, v2, op)
	case op
		when '+'
			v1+v2
		when '-'
			v1-v2
		when '/'
			v1/v2
		when '*'
			v1*v2
		when '^'
			v1**v2
	end.to_s
end
def isws (c)
	c==' ' or c=="\n" or c=="\t"
end
def isid (c)
	if c == nil
		return false
	elsif c.match(/[a-z\-A-Z0-9_~!@.#$%^&?*():\-=+]/) != nil
		return true
	else
		return false
	end
end
$css = nil
$vars = {"gehenna"=>"<small>generated with gehenna. trust me, it's as bad as it sounds</small>"}
def err (e)
	$stderr.puts "error: "+e
	exit(1)
end
def parse (doc, parent_domacro)
	macros = {}
	fn = ''
	acc = ''
	buf=:p
	hdlv = 0;
	writing = true;
	linestart = true;
	domacro = lambda { |id, params|
		if $vars.has_key?(id)
			return $vars[id]
		end
		case id
			when '.'
				if (params.length==1)
					err 'variable named "'+params[0]+'" already exists' if $vars.has_key?(params[0])
					$vars[params[0]]=''
				elsif (params.length==2)
					err 'variable named "'+params[0]+'" already exists, use `= to assign' if $vars.has_key?(params[0])
					$vars[params[0]]=parse(params[1],domacro);
				else
					err 'too many params for declaration';
				end
				return '';
			when ':'
				err 'too few params for macro' if (params.length<2)
				err 'too many params for macro' if (params.length>3)
				if params.length == 3
					macros[params[0]] = { :args => params[1].to_i, :body => params[2] };
				else
					macros[params[0]] = { :args => 0, :body => params[1] };
				end
				return ''
			when '='
				err 'wrong number of params for variable assignment' if (params.length!=2)
				v=parse(params[0],domacro)
				err "variable "+v+" undeclared!" if !$vars.has_key?(v)
				$vars[v]=parse(params[1],domacro)
				return ''
			when '?'
				err "wrong number of params for variable access" if params.length!=1
				v=parse(params[0],domacro)
				err "variable does not exist" if !$vars.has_key?(v)
				return $vars[v]
			when '0'
				if params.length==1
					v=parse(params[0],domacro)
					if !$vars.has_key?(v)
						err "variable does not exist"
					end
					vars[v] = ''
					return ''
				else
					err "wrong number of params for variable zeroing"
				end
			when '@'
				writing=!writing
				return ''
			when '+', '-', '/', '*', '^'
				if (params.length==1)
					v=parse(params[0],domacro)
					if $vars.has_key?(v)
						$vars[v]=mathop($vars[params[0]].to_i,1,id);
					else
						err 'variable named "'+params[0]+'" does not exist'
					end
				elsif (params.length==2)
					v=parse(params[0],domacro)
					if $vars.has_key?(v)
						$vars[v]=mathop($vars[params[0]].to_i,parse(params[1],domacro).to_i,id);
					else
						err 'variable named "'+params[0]+'" does not exist'
					end
				else
					err 'too many params for var declaration';
				end
				return '';
			when 'css'
				err "wrong number of args for css" if (params.length != 1)
				$css=params[0]
				return ''
			when 'wrap'
				err "wrong number of args for wrap" if (params.length<2 or params.length>3)
				tag = parse(params[0],domacro)
				content = parse(params[1],domacro) if params.length==2
				attrib = params.length==3 ? ' '+parse(params[1],domacro) : ''
				content = parse(params[2],domacro) if params.length==3
				return '<'+tag+attrib+'>' + content + '</'+tag+'>'
			when 'replace'
				err "wrong number of args for wrap" if (params.length!=3)
				haystack = parse(params[0],domacro)
				needle = parse(params[1],domacro)
				repl = parse(params[2],domacro)
				return haystack.gsub(needle,repl)
			when 'eol'
				err "wrong number of args for wrap" if (params.length!=2)
				haystack = parse(params[0],domacro)
				repl = parse(params[1],domacro)
				return haystack.gsub("\n",repl)
			when '##'
				err "wrong number of args for include" if (params.length!=2)
				return File.read(parse(params[1],domacro))
			when '#'
				err "wrong number of args for subfile" if (params.length!=2)
				return parse(File.read(parse(params[1],domacro)),domacro)
		end
		if macros.has_key?(id)
			if params.length == macros[id][:args]
				return parse macros[id][:body].gsub(/\{#([0-9]+)\}/) {
					if $1.to_i > macros[id][:args]
						err 'macro does not have that many args'
					else
						params[$1.to_i-1]
					end
				}, domacro
			else
				err 'wrong number of args for '+id+' macro ('+params.length.to_s+' for '+macros[id][:args].to_s+')'
			end
		elsif parent_domacro != nil
			return parent_domacro.call(id,params)
		end
		err "macro "+id+" not found!"
	}
	i=0;
	while i < doc.length
		if doc[i]==nil then break; end
		case doc[i]
			
			when "\n"
				if writing
					fn+="\n"
				end
				linestart = true;
			when ' ', "\t"
				if writing
					fn+=doc[i]
				end
			when '`'
				id=''
				params=[]
				i+=1
				if doc[i]=='['
					i+=1
					while doc[i]!=']'
						id+=doc[i]
						i+=1
					end
					i+=1
				else
					while isid(doc[i])
						id+=doc[i]
						i+=1
					end
				end
				while doc[i]=='['
					if doc[i+1]=="\n" #discard empty first line
						i+=2
					end
					p = ''
					cbc = 1;
					i+=1
					while cbc != 0
						err 'unmatched brace' if doc[i]==nil
						p+=doc[i]
						i+=1
						if doc[i]=="\\"
							p+=doc[i+1]
							i+=2;
						elsif doc[i]==']'
							cbc -= 1;
						elsif doc [i]=='['
							cbc += 1
						end
					end
					params.push p
					i+=1
				end
				linestart=false
				if writing
					fn += domacro.call(id,params)
				else
					domacro.call(id,params)
				end
				if doc[i]=="\n" then next;end
				i-=1
			when "\\"
				linestart=false;
				fn+=doc[i+1] if writing;
				i+=1
			else
				linestart = false;
				fn+=doc[i] if writing;
		end
		i+=1
	end
	fn
end

def markup(s)
	i = 0
	linestart = true
	bufs = {:p=>'', :hd=>'', :raw =>''}
	buf = :p
	fn = ''
	toc = []
	hdlv = 0
	flushbuf = lambda {
		if buf==:p
			if bufs[:p].length != 0
				fn += wrap('p',bufs[:p]) + "\n"
			end
			bufs[:p] = '';
		elsif buf==:hd
			fn += '<a name="h'+toc.length.to_s+'">' + wrap('h'+hdlv.to_s, bufs[:hd]) + "</a>\n";
			toc.push bufs[:hd]
			bufs[:hd]='';
			buf=:p
			hdlv=0
		elsif buf == :raw
			fn+=bufs[:raw]+"\n"
			bufs[:raw] = ''
			buf=:p
		end
	}
	while i < s.length
		case s[i]
			when "\n"
				if s[i+1] == "\n" or buf==:hd
					flushbuf.call
				end
				bufs[buf]+="\n" if bufs[buf]!=''
			when '~'
				if linestart
					flushbuf.call
					buf=:raw
				else bufs[buf]+=doc[i]; end
			when '#'
				if linestart
					hdlv += 1
					if buf!=:hd
						flushbuf.call
						buf=:hd;
					end
				else bufs[buf]+=doc[i]; end
			else
				bufs[buf] += s[i]
			end
		i +=1
	end
	if bufs[buf]!=''
		flushbuf.call
	end
	fn.gsub!(/\[(.+?)\]\((.+?)\)/) {
		'<a href="'+$2+'">'+$1+'</a>'
	}
	z = fn.gsub("<br>","<br>\n")
	fn = z if z!=nil
	toc_html = '<div class="toc">'
	i = 0
	toc.each {|t|
		toc_html += '<a href="#h'+i.to_s+'">'+t+'</a>'
		i+=1
	}
	toc_html += '</div>'
	toc_html+fn
end
puts html(markup(parse(document,nil)),$css)
